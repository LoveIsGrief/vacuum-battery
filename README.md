Foreword
========

This document currently just describes an idea of a possible new battery. As a dreamer I imagine the battery being able to acquire, contain and release the largest charges I know of - lightning and superfluous charges from wind power (which to my knowledge are currently a problem to contain).

As a naive non-physicist, for the meantime no calculations are included. I invite anybody who knows more about this area of science to join, comment and either improve, help evolve or theoretically disprove the concept.


Change log
==========

-   11 September 2011 - Convert to markdown and put on gitlab 

-   22 November 2013 - Added drawing and "maxCharge" calculations to Design \#1

-   27 October 2011 - Added a description to Design \#1

-   18 October 2011 - Initialization

TODO
====

-   Get scientific articles on this

About the battery
=================

So the basic idea is to get rid of anything inside the battery (fluids, solids, etc.) that takes up space we could use. The battery is only supposed to contain what we need and that's charge i.e electrons. To do so, before starting (the initial step) is to empty a container so that it only contains a vacuum.

Difficulties may arise when:

**Storing the charge**

-   Because the battery is supposed to be able to store any possibly large charge, of course storing it (getting it into the container )may be a problem.

-   What materials have to be used in the storing process ?

-   

-   Which storing process should be used ?

**Containing the charge**

-   What materials

-   releasing the charge: A controlled release might be a problem

In essence, quality is a big issue. The battery is supposed to be able to be used in many devices (big or small) and by many people. Among others a few factors will be important:

-   Security  
 You don't want the battery to suddenly blow up in somebody's face, release all it's charge at once, create radiation of some sort or break easily.

-   Efficiency

-   Durability  
The batter should be long lived and shouldn't use materials that slowly release charge (also an issue of security) or degrade too much (preferably not at all) with continued use.

Below a few design ideas will described and explained.

Initial design
==========

![initial design](designs/initial/drawing.svg)

Containing the charge
---------------------

The charge is supposed to be contained in a cylindrical container made of non-conductive material, that initially contains a vacuum and when fully charged only / mostly (ideally) electrons.

Storing the charge
------------------

Storing the charge is to be achieved using an electron beam that is created on one end of the container. Whatever creates the electron beam is to be known in the current main chapter as the anode.

The beam could be introduced into the container at a minimal angle towards the inner wall, so as to reduce the possibility of creating radiation when the beam hits the wall.

It could also be considered to use magnets that direct the beam in a spiral into the tube without ever touching the wall.

Releasing the charge
--------------------

The charge is to be released by attaching a consumer to the cathode to create a flow of electrons out of the cathode. The cathode is situated opposite the the anode.

To be considered
----------------

 1. Maximum, theoretical capacity

 1. Assuming the design works following factors would have to be considered:

 1. Volume of the container  
   When the non-conductive material composing and surrounding the container becomes conductive e.g assuming that ceramic, as the material composing the container, never becomes conductive and that air surrounds the battery, at some point air does become conductive (lightning).

Using \#**1** a rudimentary formula could be:

```math
chargePerElectron\ =\ -1.602176565\times10^{19}C \\

minimumDistanceBetweenElectrons\ =\ 2.07\times10^{-11}m \\

electronSphereRadius\ =\frac{minimumDistanceBeteenElectrons}{2}\ =1.035\times10^{-11}m \\

electronVolume\ =\ \frac{4}{3}\Pi\times\text{electronSphereRadius}^{3}=4.64419\ \times10^{-33}m^3 \\

numberOfElectrons\ =\ \frac{\text{containerVolumne}}{\text{electronVolume}} \\

maxCharge\ =\ chargePerElectron\times\text{\ numberOfElectrons} \\
```

An example calculation would be

```math
containerVolume = 1l = 0.001m^3 = 10^{-3}m^3\\

numberOfElectrons=\frac{0.001\ m^3}{4.64419\times10^{-33}m^3} = 2.1532\times10^{29} \\

maxCharge = -1.602176565\times10^{19}\text{C\ }\times2.1532\times10^{29} \\
maxCharge = -3.449851459565608\ \times\ 10^{48}C \\
maxCharge = -3.449851459565608\ \times\ 10^{48}A \cdot s = -9.58292072101557 * 10^{44}{A \cdot h}
```

For comparison here are average capacities for current batteries

| Battery name | Capacity ( $`A \cdot h`$)|
|--------------|-------------------------|
| Small car battery | 40 |
| Medium car battery | 50 |
| Truck battery | 75 |

To be secure the advised `maxCharge` should not exceed the charge needed to make non-conductive material conductive.

Others
======

By mistake I stumbled upon some dudes who had/have a similar [idea](http://innovate.naturemethod.com/perfect.html). Don't know when they had the idea or started working on it. 2012 maybe? This document is of 2011.



 <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/LoveIsGrief/vacuum-battery">Vacuum Battery</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/LoveIsGrief">LoveIsGrief</a> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Attribution 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p> 
